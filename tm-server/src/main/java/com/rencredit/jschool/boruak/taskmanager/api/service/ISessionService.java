package com.rencredit.jschool.boruak.taskmanager.api.service;

import com.rencredit.jschool.boruak.taskmanager.entity.Session;
import com.rencredit.jschool.boruak.taskmanager.entity.User;
import com.rencredit.jschool.boruak.taskmanager.enumerated.Role;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface ISessionService {

    void close(@Nullable final Session session);

    void closeAll(@Nullable final Session session);

    @Nullable
    User getUser(@Nullable final Session session);

    @Nullable
    String getUserId(@Nullable final Session session);

    @NotNull
    List<Session> getListSession(@Nullable final Session session);

    @Nullable
    Session sign(@Nullable final Session session);

    boolean isValid(@Nullable final Session session);

    void validate(@Nullable final Session session);

    void validate(@Nullable final Session session, @Nullable final Role role);

    @Nullable
    Session open(@Nullable final String login, @Nullable final String password);

    boolean checkDataAccess(@Nullable final String login, @Nullable final String password);

    void signOutByLogin(@Nullable final String login);

    void signOutByUserId(@Nullable final String userId);

}
