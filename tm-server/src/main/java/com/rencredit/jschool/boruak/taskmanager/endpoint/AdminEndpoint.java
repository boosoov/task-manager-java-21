package com.rencredit.jschool.boruak.taskmanager.endpoint;

import com.rencredit.jschool.boruak.taskmanager.api.endpoint.IAdminEndpoint;
import com.rencredit.jschool.boruak.taskmanager.api.locator.IServiceLocator;
import com.rencredit.jschool.boruak.taskmanager.api.service.IStorageService;
import com.rencredit.jschool.boruak.taskmanager.entity.Session;
import com.rencredit.jschool.boruak.taskmanager.enumerated.Role;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.io.IOException;

@WebService
public class AdminEndpoint extends AbstractEndpoint implements IAdminEndpoint {

    @NotNull
    private IStorageService storageService;

    public AdminEndpoint() {
    }

    public AdminEndpoint(
            @NotNull IServiceLocator serviceLocator
    ) {
        super(serviceLocator);
        storageService = serviceLocator.getStorageService();
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    @WebMethod
    public void clearBase64(
            @Nullable @WebParam(name = "session", partName = "session") final Session session
    ) throws IOException {
        sessionService.validate(session);
        authService.checkRoles(session, roles());
        storageService.clearBase64();
    }

    @Override
    @WebMethod
    public void loadBase64(
            @Nullable @WebParam(name = "session", partName = "session") final Session session
    ) throws IOException, ClassNotFoundException {
        sessionService.validate(session);
        authService.checkRoles(session, roles());
        storageService.loadBase64();
    }

    @Override
    @WebMethod
    public void saveBase64(
            @Nullable @WebParam(name = "session", partName = "session") final Session session
    ) throws IOException {
        sessionService.validate(session);
        authService.checkRoles(session, roles());
        storageService.saveBase64();
    }

    @Override
    @WebMethod
    public void clearBinary(
            @Nullable @WebParam(name = "session", partName = "session") final Session session
    ) throws IOException {
        sessionService.validate(session);
        authService.checkRoles(session, roles());
        storageService.clearBinary();
    }

    @Override
    @WebMethod
    public void loadBinary(
            @Nullable @WebParam(name = "session", partName = "session") final Session session
    ) throws IOException, ClassNotFoundException {
        sessionService.validate(session);
        authService.checkRoles(session, roles());
        storageService.loadBinary();
    }

    @Override
    @WebMethod
    public void saveBinary(
            @Nullable @WebParam(name = "session", partName = "session") final Session session
    ) throws IOException {
        sessionService.validate(session);
        authService.checkRoles(session, roles());
        storageService.saveBinary();
    }

    @Override
    @WebMethod
    public void cleanJson(
            @Nullable @WebParam(name = "session", partName = "session") final Session session
    ) throws IOException {
        sessionService.validate(session);
        authService.checkRoles(session, roles());
        storageService.cleanJson();
    }

    @Override
    @WebMethod
    public void loadJson(
            @Nullable @WebParam(name = "session", partName = "session") final Session session
    ) throws IOException {
        sessionService.validate(session);
        authService.checkRoles(session, roles());
        storageService.loadJson();
    }

    @Override
    @WebMethod
    public void saveJson(
            @Nullable @WebParam(name = "session", partName = "session") final Session session
    ) throws IOException {
        sessionService.validate(session);
        authService.checkRoles(session, roles());
        storageService.saveJson();
    }

    @Override
    @WebMethod
    public void clearXml(
            @Nullable @WebParam(name = "session", partName = "session") final Session session
    ) throws IOException {
        sessionService.validate(session);
        authService.checkRoles(session, roles());
        storageService.clearXml();
    }

    @Override
    @WebMethod
    public void loadXml(
            @Nullable @WebParam(name = "session", partName = "session") final Session session
    ) throws IOException {
        sessionService.validate(session);
        authService.checkRoles(session, roles());
        storageService.loadXml();
    }

    @Override
    @WebMethod
    public void saveXml(
            @Nullable @WebParam(name = "session", partName = "session") final Session session
    ) throws IOException {
        sessionService.validate(session);
        authService.checkRoles(session, roles());
        storageService.saveXml();
    }

    @Override
    @WebMethod
    public void shutDownServer(
            @Nullable @WebParam(name = "session", partName = "session") final Session session
    ) throws IOException {
        sessionService.validate(session);
        authService.checkRoles(session, roles());
        System.exit(0);
    }

}
