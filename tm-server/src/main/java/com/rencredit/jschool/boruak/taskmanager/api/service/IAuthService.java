package com.rencredit.jschool.boruak.taskmanager.api.service;

import com.rencredit.jschool.boruak.taskmanager.entity.Session;
import com.rencredit.jschool.boruak.taskmanager.enumerated.Role;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface IAuthService {

    void checkRoles(@NotNull Session session, @Nullable Role[] roles);

    boolean registration(@Nullable String login, @Nullable String password);

    boolean registration(@Nullable String login, @Nullable String password, @Nullable String email);

    boolean registration(@Nullable String login, @Nullable String password, @Nullable Role role);

}
