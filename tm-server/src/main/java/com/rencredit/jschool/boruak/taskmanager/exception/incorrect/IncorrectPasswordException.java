package com.rencredit.jschool.boruak.taskmanager.exception.incorrect;

public class IncorrectPasswordException extends RuntimeException {

    public IncorrectPasswordException() {
        super("Error! Password incorrect...");
    }

}
