package com.rencredit.jschool.boruak.taskmanager.endpoint;

import com.rencredit.jschool.boruak.taskmanager.api.endpoint.IAuthEndpoint;
import com.rencredit.jschool.boruak.taskmanager.api.locator.IServiceLocator;
import com.rencredit.jschool.boruak.taskmanager.api.service.IAuthService;
import com.rencredit.jschool.boruak.taskmanager.dto.Fail;
import com.rencredit.jschool.boruak.taskmanager.dto.Result;
import com.rencredit.jschool.boruak.taskmanager.dto.Success;
import com.rencredit.jschool.boruak.taskmanager.entity.Session;
import com.rencredit.jschool.boruak.taskmanager.enumerated.Role;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public class AuthEndpoint extends AbstractEndpoint implements IAuthEndpoint {

    @NotNull
    private IAuthService authService;

    public AuthEndpoint() {
    }

    public AuthEndpoint(
            @NotNull IServiceLocator serviceLocator
    ) {
        super(serviceLocator);
        authService = serviceLocator.getAuthService();
    }

    @NotNull
    @Override
    @WebMethod
    public Session logIn(
            @Nullable @WebParam(name = "login", partName = "login") final String login,
            @Nullable @WebParam(name = "password", partName = "password") final String password
    ) {
        @NotNull final Session session = sessionService.open(login, password);
        return session;
    }

    @NotNull
    @Override
    @WebMethod
    public Result logOut(
            @Nullable @WebParam(name = "session", partName = "session") final Session session
    ) {
        sessionService.validate(session);
        sessionService.validate(session);
        try {
            sessionService.close(session);
            return new Success();
        } catch (final Exception e) {
            return new Fail(e);
        }
    }

    @Nullable
    @Override
    @WebMethod
    public String getUserId(
            @Nullable @WebParam(name = "session", partName = "session") final Session session
    ) {
        sessionService.validate(session);
        return session.getUserId();
    }

    @Override
    @WebMethod
    public void checkRoles(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "roles", partName = "roles") final Role[] roles
    ) {
        sessionService.validate(session);
        authService.checkRoles(session, roles);
    }

    @Override
    @WebMethod
    public boolean registrationLoginPassword(
            @Nullable @WebParam(name = "login", partName = "login") final String login,
            @Nullable @WebParam(name = "password", partName = "password") final String password
    ) {
        return authService.registration(login, password);
    }

    @Override
    @WebMethod
    public boolean registrationLoginPasswordEmail(
            @Nullable @WebParam(name = "login", partName = "login") final String login,
            @Nullable @WebParam(name = "password", partName = "password") final String password,
            @Nullable @WebParam(name = "email", partName = "email") final String email
    ) {
        return authService.registration(login, password, email);
    }

    @Override
    @WebMethod
    public boolean registrationLoginPasswordRole(
            @Nullable @WebParam(name = "login", partName = "login") final String login,
            @Nullable @WebParam(name = "password", partName = "password") final String password,
            @Nullable @WebParam(name = "role", partName = "role") final Role role
    ) {
        return authService.registration(login, password, role);
    }

}
