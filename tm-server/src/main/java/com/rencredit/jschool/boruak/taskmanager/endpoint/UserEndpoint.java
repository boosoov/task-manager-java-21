package com.rencredit.jschool.boruak.taskmanager.endpoint;

import com.rencredit.jschool.boruak.taskmanager.api.endpoint.IUserEndpoint;
import com.rencredit.jschool.boruak.taskmanager.api.locator.IServiceLocator;
import com.rencredit.jschool.boruak.taskmanager.api.service.IUserService;
import com.rencredit.jschool.boruak.taskmanager.entity.Session;
import com.rencredit.jschool.boruak.taskmanager.entity.User;
import com.rencredit.jschool.boruak.taskmanager.enumerated.Role;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;
import java.util.List;

@WebService
public class UserEndpoint extends AbstractEndpoint implements IUserEndpoint {

    @NotNull
    private IUserService userService;

    public UserEndpoint() {
    }

    public UserEndpoint(
            @NotNull IServiceLocator serviceLocator
    ) {
        super(serviceLocator);
        userService = serviceLocator.getUserService();
    }

    @Nullable
    @Override
    @WebMethod
    public User getUserById(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "id", partName = "id") String id
    ) {
        sessionService.validate(session);
        return userService.getById(id);
    }

    @Nullable
    @Override
    @WebMethod
    public User getUserByLogin(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "login", partName = "login") String login
    ) {
        sessionService.validate(session);
        return userService.getByLogin(login);
    }

    @Override
    @WebMethod
    public boolean addUserLoginPassword(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "login", partName = "login") String login,
            @Nullable @WebParam(name = "password", partName = "password") String password
    ) {
        sessionService.validate(session);
        return userService.add(login, password);
    }

    @Override
    @WebMethod
    public boolean addUserLoginPasswordFirstName(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "login", partName = "login") String login,
            @Nullable @WebParam(name = "password", partName = "password") String password,
            @Nullable @WebParam(name = "firstName", partName = "firstName") String firstName
    ) {
        sessionService.validate(session);
        return userService.add(login, password, firstName);
    }

    @Override
    @WebMethod
    public boolean addUserLoginPasswordRole(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "login", partName = "login") String login,
            @Nullable @WebParam(name = "password", partName = "password") String password,
            @Nullable @WebParam(name = "role", partName = "role") Role role
    ) {
        sessionService.validate(session);
        return userService.add(login, password, role);
    }

    @Nullable
    @Override
    @WebMethod
    public User editUserProfileByIdFirstName(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "id", partName = "id") String id,
            @Nullable @WebParam(name = "firstName", partName = "firstName") String firstName
    ) {
        sessionService.validate(session);
        return userService.editProfileById(id, firstName);
    }

    @Nullable
    @Override
    @WebMethod
    public User editUserProfileByIdFirstNameLastName(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "id", partName = "id") String id,
            @Nullable @WebParam(name = "firstName", partName = "firstName") String firstName,
            @Nullable @WebParam(name = "lastName", partName = "lastName") String lastName
    ) {
        sessionService.validate(session);
        return userService.editProfileById(id, firstName, lastName);
    }

    @Nullable
    @Override
    @WebMethod
    public User editUserProfileByIdFirstNameLastNameMiddleName(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "id", partName = "id") String id,
            @Nullable @WebParam(name = "email", partName = "email") String email,
            @Nullable @WebParam(name = "firstName", partName = "firstName") String firstName,
            @Nullable @WebParam(name = "lastName", partName = "lastName") String lastName,
            @Nullable @WebParam(name = "middleName", partName = "middleName") String middleName
    ) {
        sessionService.validate(session);
        return userService.editProfileById(id, email, firstName, lastName, middleName);
    }

    @Nullable
    @Override
    @WebMethod
    public User updateUserPasswordById(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "id", partName = "id") String id,
            @Nullable @WebParam(name = "newPassword", partName = "newPassword") String newPassword
    ) {
        sessionService.validate(session);
        return userService.updatePasswordById(id, newPassword);
    }

    @Nullable
    @Override
    @WebMethod
    public User removeUserById(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "id", partName = "id") String id
    ) {
        sessionService.validate(session);
        return userService.removeById(id);
    }

    @Nullable
    @Override
    @WebMethod
    public User removeUserByLogin(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "login", partName = "login") String login
    ) {
        sessionService.validate(session);
        return userService.removeByLogin(login);
    }

    @Nullable
    @Override
    @WebMethod
    public User removeUserByUser(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "user", partName = "user") User user
    ) {
        sessionService.validate(session);
        return userService.removeByUser(user);
    }

    @Override
    @WebMethod
    public void loadUserCollection(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "elements", partName = "elements") Collection<User> elements
    ) {
        sessionService.validate(session);
        userService.load(elements);
    }

    @Override
    @WebMethod
    public void loadUserVararg(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "elements", partName = "elements") User... elements
    ) {
        sessionService.validate(session);
        userService.load(elements);
    }

    @Override
    @WebMethod
    public boolean mergeUser(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "element", partName = "element") User element
    ) {
        sessionService.validate(session);
        return userService.merge(element);
    }

    @Override
    @WebMethod
    public void mergeUserCollection(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "elements", partName = "elements") Collection<User> elements
    ) {
        sessionService.validate(session);
        userService.merge(elements);
    }

    @Override
    @WebMethod
    public void mergeUserVararg(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "elements", partName = "elements") User... elements
    ) {
        sessionService.validate(session);
        userService.merge(elements);
    }

    @Override
    @WebMethod
    public void clearAllUser(
            @Nullable @WebParam(name = "session", partName = "session") final Session session
    ) {
        sessionService.validate(session);
        userService.clearAll();
    }

    @NotNull
    @Override
    @WebMethod
    public List<User> getUserList(
            @Nullable @WebParam(name = "session", partName = "session") final Session session
    ) {
        sessionService.validate(session);
        return userService.getList();
    }

}
