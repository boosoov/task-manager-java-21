package com.rencredit.jschool.boruak.taskmanager.service;

import com.rencredit.jschool.boruak.taskmanager.api.repository.IUserRepository;
import com.rencredit.jschool.boruak.taskmanager.api.service.IUserService;
import com.rencredit.jschool.boruak.taskmanager.entity.User;
import com.rencredit.jschool.boruak.taskmanager.enumerated.Role;
import com.rencredit.jschool.boruak.taskmanager.exception.busy.BusyLoginException;
import com.rencredit.jschool.boruak.taskmanager.exception.denied.DeniedAccessException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.*;
import com.rencredit.jschool.boruak.taskmanager.util.HashUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class UserService extends AbstractService<User> implements IUserService {

    @NotNull
    private final IUserRepository userRepository;

    public UserService(@NotNull final IUserRepository userRepository) {
        super(userRepository);
        this.userRepository = userRepository;
    }

    @Nullable
    @Override
    public User getById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return userRepository.findById(id);
    }

    @Nullable
    @Override
    public User getByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        return userRepository.findByLogin(login);
    }

    @Override
    public boolean add(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();

        @Nullable final String passwordHash = HashUtil.getHashLine(password);
        if (passwordHash == null) throw new DeniedAccessException();
        @NotNull final User user = new User(login, passwordHash);
        return add(login, user);
    }

    @Override
    public boolean add(@Nullable final String login, @Nullable final String password, @Nullable final String firstName) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (firstName == null || firstName.isEmpty()) throw new EmptyFirstNameException();

        @Nullable final String passwordHash = HashUtil.getHashLine(password);
        if (passwordHash == null) throw new DeniedAccessException();
        @NotNull final User user = new User(login, passwordHash, firstName);
        return add(login, user);
    }

    @Override
    public boolean add(@Nullable final String login, @Nullable final String password, @Nullable final Role role) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (role == null) throw new EmptyRoleException();

        @Nullable final String passwordHash = HashUtil.getHashLine(password);
        if (passwordHash == null) throw new DeniedAccessException();
        @NotNull final User user = new User(login, passwordHash, role);
        return add(login, user);
    }

    private boolean add(@Nullable final String login, @Nullable final User user) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (user == null) throw new EmptyUserException();

        if (userRepository.findByLogin(login) != null) throw new BusyLoginException();
        return userRepository.add(user);
    }

    @Nullable
    @Override
    public User updatePasswordById(@Nullable final String id, @Nullable final String newPassword) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (newPassword == null || newPassword.isEmpty()) throw new EmptyNewPasswordException();

        @Nullable final String passwordHash = HashUtil.getHashLine(newPassword);
        if (passwordHash == null) throw new DeniedAccessException();
        @Nullable final User user = userRepository.findById(id);
        if (user == null) throw new EmptyUserException();
        user.setPasswordHash(passwordHash);
        return user;
    }

    @Nullable
    @Override
    public User editProfileById(@Nullable final String id, @Nullable final String firstName) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (firstName == null || firstName.isEmpty()) throw new EmptyFirstNameException();

        @Nullable final User user = userRepository.findById(id);
        if (user == null) throw new EmptyUserException();
        user.setFirstName(firstName);
        return user;
    }

    @Nullable
    @Override
    public User editProfileById(
            @Nullable final String id,
            @Nullable final String firstName,
            @Nullable final String lastName
    ) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (firstName == null || firstName.isEmpty()) throw new EmptyFirstNameException();
        if (lastName == null || lastName.isEmpty()) throw new EmptyLastNameException();

        @Nullable final User user = userRepository.findById(id);
        if (user == null) throw new EmptyUserException();
        user.setFirstName(firstName);
        user.setLastName(lastName);
        return user;
    }

    @Nullable
    @Override
    public User editProfileById(
            @Nullable final String id,
            @Nullable final String email,
            @Nullable final String firstName,
            @Nullable final String lastName,
            @Nullable final String middleName
    ) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        if (firstName == null || firstName.isEmpty()) throw new EmptyFirstNameException();
        if (lastName == null || lastName.isEmpty()) throw new EmptyLastNameException();
        if (middleName == null || middleName.isEmpty()) throw new EmptyMiddleNameException();

        @Nullable final User user = userRepository.findById(id);
        if (user == null) throw new EmptyUserException();
        user.setEmail(email);
        user.setFirstName(firstName);
        user.setFirstName(lastName);
        user.setMiddleName(middleName);
        return user;
    }

    @Nullable
    @Override
    public User removeById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return userRepository.removeById(id);
    }

    @Nullable
    @Override
    public User removeByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        return userRepository.removeByLogin(login);
    }

    @Override
    public User removeByUser(@Nullable final User user) {
        if (user == null) throw new EmptyUserException();
        return userRepository.removeByUser(user);
    }

    @NotNull
    @Override
    public User lockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @Nullable final User user = userRepository.findByLogin(login);
        if (user == null) throw new EmptyUserException();
        user.setLocked(true);
        return user;
    }

    @NotNull
    @Override
    public User unlockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @Nullable final User user = userRepository.findByLogin(login);
        if (user == null) throw new EmptyUserException();
        user.setLocked(false);
        return user;
    }

}
