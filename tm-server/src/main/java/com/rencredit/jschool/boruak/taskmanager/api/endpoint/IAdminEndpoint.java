package com.rencredit.jschool.boruak.taskmanager.api.endpoint;

import com.rencredit.jschool.boruak.taskmanager.entity.Session;
import org.jetbrains.annotations.Nullable;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.io.IOException;

public interface IAdminEndpoint {

    @WebMethod
    void clearBase64(
            @Nullable @WebParam(name = "session", partName = "session") final Session session
    ) throws IOException;

    @WebMethod
    void loadBase64(
            @Nullable @WebParam(name = "session", partName = "session") final Session session
    ) throws IOException, ClassNotFoundException;

    @WebMethod
    void saveBase64(
            @Nullable @WebParam(name = "session", partName = "session") final Session session
    ) throws IOException;

    @WebMethod
    void clearBinary(
            @Nullable @WebParam(name = "session", partName = "session") final Session session
    ) throws IOException;

    @WebMethod
    void loadBinary(
            @Nullable @WebParam(name = "session", partName = "session") final Session session
    ) throws IOException, ClassNotFoundException;

    @WebMethod
    void saveBinary(
            @Nullable @WebParam(name = "session", partName = "session") final Session session
    ) throws IOException;

    @WebMethod
    void cleanJson(
            @Nullable @WebParam(name = "session", partName = "session") final Session session
    ) throws IOException;

    @WebMethod
    void loadJson(
            @Nullable @WebParam(name = "session", partName = "session") final Session session
    ) throws IOException;

    @WebMethod
    void saveJson(
            @Nullable @WebParam(name = "session", partName = "session") final Session session
    ) throws IOException;

    @WebMethod
    void clearXml(
            @Nullable @WebParam(name = "session", partName = "session") final Session session
    ) throws IOException;

    @WebMethod
    void loadXml(
            @Nullable @WebParam(name = "session", partName = "session") final Session session
    ) throws IOException;

    @WebMethod
    void saveXml(
            @Nullable @WebParam(name = "session", partName = "session") final Session session
    ) throws IOException;

    @WebMethod
    void shutDownServer(
            @Nullable @WebParam(name = "session", partName = "session") final Session session
    ) throws IOException;

}
