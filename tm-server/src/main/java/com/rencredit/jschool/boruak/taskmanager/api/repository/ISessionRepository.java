package com.rencredit.jschool.boruak.taskmanager.api.repository;

import com.rencredit.jschool.boruak.taskmanager.entity.Session;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface ISessionRepository extends IRepository<Session> {

    @Nullable
    Session removeByUserId(@NotNull String user);

    @Nullable
    Session removeBySession(@NotNull final Session session);

    @Nullable
    Session findByUserId(@NotNull String user);

    boolean contains(@NotNull String user);

}
