package com.rencredit.jschool.boruak.taskmanager.entity;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import java.io.Serializable;
import java.util.UUID;

@Setter
@Getter
public class AbstractEntity implements Serializable {

    @NotNull
    private String id = UUID.randomUUID().toString();

}
