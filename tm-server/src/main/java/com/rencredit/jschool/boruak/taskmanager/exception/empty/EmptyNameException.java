package com.rencredit.jschool.boruak.taskmanager.exception.empty;

public class EmptyNameException extends RuntimeException {

    public EmptyNameException() {
        super("Error! Name is empty...");
    }

}
