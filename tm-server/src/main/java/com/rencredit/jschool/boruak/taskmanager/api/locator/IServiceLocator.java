package com.rencredit.jschool.boruak.taskmanager.api.locator;

import com.rencredit.jschool.boruak.taskmanager.api.service.*;
import org.jetbrains.annotations.NotNull;

public interface IServiceLocator {

    @NotNull
    IUserService getUserService();

    @NotNull
    IAuthService getAuthService();

    @NotNull
    ITaskService getTaskService();

    @NotNull
    IProjectService getProjectService();

    @NotNull
    IDomainService getDomainService();

    @NotNull
    ISessionService getSessionService();

    @NotNull
    IPropertyService getPropertyService();

    @NotNull
    IStorageService getStorageService();

}
