package com.rencredit.jschool.boruak.taskmanager.service;

import com.rencredit.jschool.boruak.taskmanager.api.repository.IRepository;
import com.rencredit.jschool.boruak.taskmanager.api.service.IService;
import com.rencredit.jschool.boruak.taskmanager.entity.AbstractEntity;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.EmptyUserException;
import com.rencredit.jschool.boruak.taskmanager.exception.notexist.NotExistAbstractListException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;
import java.util.List;

public class AbstractService<E extends AbstractEntity> implements IService<E> {

    @NotNull
    protected final IRepository<E> repository;

    public AbstractService(@NotNull final IRepository<E> repository) {
        this.repository = repository;
    }

    @Override
    public void load(@Nullable final Collection<E> elements) {
        if (elements != null) repository.load(elements);
    }

    @Override
    public void load(@Nullable final E... elements) {
        if (elements != null) repository.load(elements);
    }

    @Override
    public boolean merge(@Nullable final E element) {
        if (element == null) throw new EmptyUserException();
        return repository.merge(element);
    }

    @Override
    public void merge(@Nullable final Collection<E> elements) {
        if (elements == null) throw new NotExistAbstractListException();
        repository.merge(elements);
    }

    @Override
    public void merge(@Nullable final E... elements) {
        if (elements == null) throw new NotExistAbstractListException();
        repository.merge(elements);
    }

    @Override
    public void clearAll() {
        repository.clearAll();
    }

    @NotNull
    @Override
    public List<E> getList() {
        return repository.getList();
    }

}
