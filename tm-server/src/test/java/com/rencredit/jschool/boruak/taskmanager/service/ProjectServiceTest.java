package com.rencredit.jschool.boruak.taskmanager.service;

import com.rencredit.jschool.boruak.taskmanager.exception.empty.EmptyUserIdException;
import com.rencredit.jschool.boruak.taskmanager.repository.ProjectRepository;
import org.junit.Test;

public class ProjectServiceTest {

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeCreateWithoutUserId() {
        final ProjectRepository projectRepository = new ProjectRepository();
        final ProjectService projectService = new ProjectService(projectRepository);
        projectService.create(null, "Demo");
    }

}
