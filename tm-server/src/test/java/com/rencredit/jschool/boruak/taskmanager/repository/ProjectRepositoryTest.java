package com.rencredit.jschool.boruak.taskmanager.repository;

import com.rencredit.jschool.boruak.taskmanager.entity.Project;
import org.junit.Assert;
import org.junit.Test;

public class ProjectRepositoryTest {

    @Test
    public void testCreate() {
        final ProjectRepository projectRepository = new ProjectRepository();
        Assert.assertTrue(projectRepository.getList().isEmpty());
        final Project project1 = new Project();
        projectRepository.add(project1);
        final Project projectFromRepository = projectRepository.getList().get(0);
        Assert.assertNotNull(projectFromRepository);
        Assert.assertNotNull(projectFromRepository.getId());
        Assert.assertNotNull(projectFromRepository.getName());
    }

}
