package com.rencredit.jschool.boruak.taskmanager.exception.empty;

public class EmptyUserException extends RuntimeException {

    public EmptyUserException() {
        super("Error! User is empty...");
    }

}
