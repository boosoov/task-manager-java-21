package com.rencredit.jschool.boruak.taskmanager.exception.empty;

public class EmptyHashLineException extends RuntimeException {

    public EmptyHashLineException() {
        super("Error! Line for hash is empty...");
    }

}
