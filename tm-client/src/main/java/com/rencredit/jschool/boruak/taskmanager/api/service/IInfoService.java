package com.rencredit.jschool.boruak.taskmanager.api.service;

import org.jetbrains.annotations.NotNull;

public interface IInfoService {

    @NotNull
    String getArgumentList();

    @NotNull
    String getCommandList();

    @NotNull
    String getDeveloperInfo();

    @NotNull
    String getHelp();

    @NotNull
    String getSystemInfo();

    @NotNull
    String getVersion();

}
