package com.rencredit.jschool.boruak.taskmanager.api.service;

import com.rencredit.jschool.boruak.taskmanager.endpoint.Session;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface ISystemObjectService {

    @Nullable
    Session putSession(@Nullable Session session);

    @NotNull
    Session getSession();

    @Nullable
    Session removeSession();

    void clearAll();

}
