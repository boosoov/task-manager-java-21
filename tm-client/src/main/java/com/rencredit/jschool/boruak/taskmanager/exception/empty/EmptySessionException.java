package com.rencredit.jschool.boruak.taskmanager.exception.empty;

public class EmptySessionException extends RuntimeException {

    public EmptySessionException() {
        super("Error! Session is empty...");
    }

}
