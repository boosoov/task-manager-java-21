package com.rencredit.jschool.boruak.taskmanager.repository;

import com.rencredit.jschool.boruak.taskmanager.api.repository.ISystemObjectRepository;
import com.rencredit.jschool.boruak.taskmanager.endpoint.Session;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.HashMap;
import java.util.Map;

public class SystemObjectRepository implements ISystemObjectRepository {

    @NotNull
    private final Map<String, Object> objects = new HashMap<>();

    @Nullable
    @Override
    public Session putSession(@NotNull final Session session) {
        return (Session) objects.put("Session", session);
    }

    @Nullable
    @Override
    public Session getSession() {
        return (Session) objects.get("Session");
    }

    @Nullable
    @Override
    public Session removeSession() {
        return (Session) objects.remove("Session");
    }

    @Override
    public void clearAll() {
        objects.clear();
    }

}
