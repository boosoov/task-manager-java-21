package com.rencredit.jschool.boruak.taskmanager.exception.notexist;

public class NotExistTasksListException extends RuntimeException {

    public NotExistTasksListException() {
        super("Error! Task list is not exist...");
    }

}
