package com.rencredit.jschool.boruak.taskmanager.locator;

import com.rencredit.jschool.boruak.taskmanager.api.locator.IServiceLocator;
import com.rencredit.jschool.boruak.taskmanager.api.repository.ICommandRepository;
import com.rencredit.jschool.boruak.taskmanager.api.repository.ISystemObjectRepository;
import com.rencredit.jschool.boruak.taskmanager.api.service.ICommandService;
import com.rencredit.jschool.boruak.taskmanager.api.service.IInfoService;
import com.rencredit.jschool.boruak.taskmanager.api.service.ISystemObjectService;
import com.rencredit.jschool.boruak.taskmanager.repository.CommandRepository;
import com.rencredit.jschool.boruak.taskmanager.repository.SystemObjectRepository;
import com.rencredit.jschool.boruak.taskmanager.service.CommandService;
import com.rencredit.jschool.boruak.taskmanager.service.InfoService;
import com.rencredit.jschool.boruak.taskmanager.service.SystemObjectService;
import org.jetbrains.annotations.NotNull;

public class ServiceLocator implements IServiceLocator {

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @NotNull
    private final IInfoService infoService = new InfoService(this);

    @NotNull
    private final ISystemObjectRepository systemObjectRepository = new SystemObjectRepository();

    @NotNull
    private final ISystemObjectService systemObjectService = new SystemObjectService(systemObjectRepository);

    @NotNull
    @Override
    public ICommandService getCommandService() {
        return commandService;
    }

    @NotNull
    @Override
    public IInfoService getInfoService() {
        return infoService;
    }

    @NotNull
    @Override
    public ISystemObjectService getSystemObjectService() {
        return systemObjectService;
    }

}
