package com.rencredit.jschool.boruak.taskmanager.command.data;

import com.rencredit.jschool.boruak.taskmanager.command.AbstractCommand;
import com.rencredit.jschool.boruak.taskmanager.endpoint.IOException_Exception;
import com.rencredit.jschool.boruak.taskmanager.endpoint.Session;
import com.rencredit.jschool.boruak.taskmanager.enumerated.Role;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class DataXmlSaveCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "data-xml-save";
    }

    @NotNull
    @Override
    public String description() {
        return "Save data to xml file";
    }

    @Override
    public void execute() throws IOException_Exception {
        @NotNull final Session session = serviceLocator.getSystemObjectService().getSession();
        endpointLocator.getAdminEndpoint().saveXml(session);
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
