package com.rencredit.jschool.boruak.taskmanager.command.system;

import com.rencredit.jschool.boruak.taskmanager.command.AbstractCommand;
import org.jetbrains.annotations.NotNull;

public class SystemExitCommand extends AbstractCommand {

    @NotNull
    @Override
    public String arg() {
        return "-e";
    }

    @NotNull
    @Override
    public String name() {
        return "exit";
    }

    @NotNull
    @Override
    public String description() {
        return "Exit program.";
    }

    @Override
    public void execute() {
        System.exit(0);
    }

}
