package com.rencredit.jschool.boruak.taskmanager.api.locator;

import com.rencredit.jschool.boruak.taskmanager.endpoint.*;
import org.jetbrains.annotations.NotNull;

public interface IEndpointLocator {

    @NotNull
    UserEndpointService getUserEndpointService();

    @NotNull
    UserEndpoint getUserEndpoint();

    @NotNull
    SessionEndpoint getSessionEndpoint();

    @NotNull
    TaskEndpoint getTaskEndpoint();

    @NotNull
    ProjectEndpoint getProjectEndpoint();

    @NotNull
    AdminEndpoint getAdminEndpoint();

    @NotNull
    AdminUserEndpoint getAdminUserEndpoint();

    @NotNull
    AuthEndpoint getAuthEndpoint();

}
