package com.rencredit.jschool.boruak.taskmanager.service;

import com.rencredit.jschool.boruak.taskmanager.api.repository.ISystemObjectRepository;
import com.rencredit.jschool.boruak.taskmanager.api.service.ISystemObjectService;
import com.rencredit.jschool.boruak.taskmanager.endpoint.Session;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.EmptySessionException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class SystemObjectService implements ISystemObjectService {

    @NotNull
    private final ISystemObjectRepository systemObjectRepository;

    public SystemObjectService(@NotNull final ISystemObjectRepository systemObjectRepository) {
        this.systemObjectRepository = systemObjectRepository;
    }

    @Nullable
    @Override
    public Session putSession(@Nullable final Session session) {
        if(session == null) throw new EmptySessionException();
        @Nullable final Session lastSession = systemObjectRepository.putSession(session);
        return lastSession;
    }

    @NotNull
    @Override
    public Session getSession() {
        @Nullable final Session session = systemObjectRepository.getSession();
        if(session == null) throw new EmptySessionException();
        return session;
    }

    @Nullable
    @Override
    public Session removeSession() {
        @Nullable final Session session = systemObjectRepository.removeSession();
        return session;
    }

    @Override
    public void clearAll() {
        systemObjectRepository.clearAll();
    }

}
