package com.rencredit.jschool.boruak.taskmanager.endpoint;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;

public class SessionEndpointTest {

    @NotNull
    private final SessionEndpointService sessionEndpointService = new SessionEndpointService();

    @NotNull
    private final SessionEndpoint sessionEndpoint = sessionEndpointService.getSessionEndpointPort();

    @Test
    public void testOpenSession() {
        final Session session = sessionEndpoint.openSession("test", "test");
        Assert.assertNotNull(session);
    }

    @Test
    public void testOpenSessionIncorrectLogin() {
        final Session session = sessionEndpoint.openSession("test564", "test345");
        Assert.assertNull(session);
    }

}
